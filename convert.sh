#!/bin/bash

SOURCE=/mnt/d/Pictures/
DESTINATION=/mnt/d/Pictures/

cd $SOURCE
for SOURCE_DIRECTORY in */ ; do

	if [ "$SOURCE_DIRECTORY" == "2005-08-27 - 2020-04-24/" ]; then
		continue
	fi

    echo "Migrating $SOURCE_DIRECTORY"

    # DESTINATION_DIRECTORY="${SOURCE_DIRECTORY/ batch2/}"
    DESTINATION_DIRECTORY=${SOURCE_DIRECTORY}

    if [ ! -d "$DESTINATION$DESTINATION_DIRECTORY" ]; then
    	mkdir "$DESTINATION$DESTINATION_DIRECTORY"
    fi

    cd "$SOURCE$SOURCE_DIRECTORY"
    for FILENAME in *; do
    	FROM_FILENAME="$FILENAME"
    	TO_FILENAME="${FILENAME/%.HEIC/.JPG}"
    	# echo Converting $FROM_FILENAME to $TO_FILENAME
    	if [ "$FROM_FILENAME" != "$TO_FILENAME" ]; then
    		# We must be doing a conversion

			# Convert the file using tifig
    		if [ ! -e "$DESTINATION$DESTINATION_DIRECTORY${TO_FILENAME}" ]; then
    			echo Converting "$SOURCE$SOURCE_DIRECTORY$FROM_FILENAME" to "$DESTINATION$DESTINATION_DIRECTORY${TO_FILENAME}"
	    		/home/rickm/bin/tifig -p "$SOURCE$SOURCE_DIRECTORY$FROM_FILENAME" "$DESTINATION$DESTINATION_DIRECTORY${TO_FILENAME}" # 2>&1
	    	fi

			# Create the HEIC folder if it's not there
			HEIC_DIRECTORY="$DESTINATION${DESTINATION_DIRECTORY}HEIC"
		    if [ ! -d "$HEIC_DIRECTORY" ]; then
				echo Creating HEIC directory "$HEIC_DIRECTORY"
		    	mkdir "$HEIC_DIRECTORY"
		    fi

			# Move the HEIC file into the HEIC directory
    		if [ ! -e "$HEIC_DIRECTORY/${FROM_FILENAME}" ]; then
				echo "Moving $FROM_FILENAME into $HEIC_DIRECTORY"
	    		mv "$SOURCE$SOURCE_DIRECTORY$FROM_FILENAME" "$HEIC_DIRECTORY/${FROM_FILENAME}"
			fi
    	else
			# Filenames are the same so we just move the file from source to destination
    		if [ ! -e "$DESTINATION$DESTINATION_DIRECTORY${TO_FILENAME}" ]; then
				echo "Moving non HEIC file into $DESTINATION$DESTINATION_DIRECTORY"
	    		cp "$SOURCE$SOURCE_DIRECTORY$FROM_FILENAME" "$DESTINATION$DESTINATION_DIRECTORY${TO_FILENAME}"
	    	fi
    	fi
    done
done


